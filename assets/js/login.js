
(function ($) {
    "use strict";

    
    /*==================================================================
    [ Validate ]*/
    var inputLogin = $('.validate-form-login .input100');
    var inputSignUp = $('.validate-form-signup .input100');
    var inputForgot = $('.validate-form-forgot .input100');

    $('.validate-form-login').on('submit',function(){
        var checkLogin = true;

        for(var i=0; i<inputLogin.length; i++) {
            if(validate(inputLogin[i]) == false){
                showValidate(inputLogin[i]);
                checkLogin=false;
            }
        }
        return checkLogin;
    });


    $('.validate-form-login .input100').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });
    
    $('.validate-form-signup').on('submit',function(){
        var checkSignUp = true;

        for(var i=0; i<inputSignUp.length; i++) {
            if(validate(inputSignUp[i]) == false){
                showValidate(inputSignUp[i]);
                checkSignUp = false;
            }
        }

        return checkSignUp;
    });


    $('.validate-form-signup .input100').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });
    
    $('.validate-form-forgot').on('submit',function(){
        var checkForgot = true;

        for(var i=0; i<inputForgot.length; i++) {
            if(validate(inputForgot[i]) == false){
                showValidate(inputForgot[i]);
                checkForgot = false;
            }
        }

        return checkForgot;
    });


    $('.validate-form-forgot .input100').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function validate (input) {
        if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }
        else {
            if($(input).val().trim() == ''){
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }
    
    $('.cancel100-form-btn').on('click', () => {
        $('.login100-form').removeClass('d-none');
        $('.forgot100-form').addClass('d-none');
        $('.signup100-form').addClass('d-none');
        
        
    });
    
    
    $('.create-acc').on('click', () => {
        $('.signup100-form').removeClass('d-none');
        $('.login100-form').addClass('d-none');
    });
    
    $('.forgot-password').on('click', () => {
        $('.forgot100-form').removeClass('d-none');
        $('.login100-form').addClass('d-none');
    });



})(jQuery);