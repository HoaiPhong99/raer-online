$(document).ready(() => {

    var menu = false;
    $(document).on('click', '[data-menu]', () => {
        var aside = document.querySelector('aside > section')
        aside.classList.toggle('active');

        menu = !menu;
        if (menu) {
            $('body').append('<div id="overlay"></div>')
        } else {
            $('#overlay').remove()
        }

    })

    $(document).on('click', '#overlay', () => {
        menu = false;
        $('#overlay').remove();
        document.querySelector('aside > section').classList.remove('active')
    })

    // acction choose
    var dataChoose = false;
    $(document).on('click', '[data-choose]', (e) => {
        const choose = document.querySelector('.action-choose > ul');
        dataChoose = !dataChoose
        if (dataChoose) {
            choose.classList.remove('d-none');
            document.getElementById('img-add').style.transform = "rotate(230deg)";
        } else {
            choose.classList.add('d-none');
            document.getElementById('img-add').style.transform = "";
        }
    })

    var acctionChoose = false;
    $('.action > img').on('click', function () {
        // console.log(this.length);
        acctionChoose = !acctionChoose;
        if (acctionChoose) {
            $('.action-menu').addClass('d-none');
            $(this).next().removeClass('d-none');
        } else {
            $(this).next().addClass('d-none');
        }
        // console.log('hello');
    })

    //select image
    $(".select-wallets").change(function () {        
        $('.img-select').children().attr({
            src: './assets/media/images/wallets/' + $(this).val() + '.svg',
            width: '35rem'
        });
    })

    function format(state) {
        if (!state.id) return state.text; // optgroup
        return "<img class='flag' src='./assets/media/images/wallets/" + state.id.toLowerCase() + ".svg'/>" + state.text;
    }
    //select wallets (to Wallets)
    $(".select-wallets").select2({
        placeholder: "Select your wallets",
        allowClear: true,
        dropdownAutoWidth: true,
        width: '100%',
        templateResult: format,
        escapeMarkup: function (format) { return format; }
    });

    //select image
    $(".select-toWallets").change(function () {        
        $('.img-toWallets').children().attr({
            src: './assets/media/images/wallets/' + $(this).val() + '.svg',
            width: '35rem'
        });
    })

    function formatToWallest(state) {
        if (!state.id) return state.text; // optgroup
        return "<img class='flag' src='./assets/media/images/wallets/" + state.id.toLowerCase() + ".svg'/>" + state.text;
    }
    //select wallets (to Wallets)
    $(".select-toWallets").select2({
        placeholder: {
            id: '-1', // the value of the option
            text: 'Select an option'
        },
        allowClear: true,
        dropdownAutoWidth: true,
        width: '100%',
        templateResult: formatToWallest,
        escapeMarkup: function (formatToWallest) { return formatToWallest; }
    });



    //select image
    $(".select-category").change(function () {        
        $('.img-category').children().attr('src', './assets/media/images/category/' + $(this).val() + '.svg');
    })


    //select category expense
    function formatCategory(state) {
        if (!state.id) return state.text; // optgroup
        return "<img class='flag' src='./assets/media/images/category/" + state.id.toLowerCase() + ".svg'/>" + state.text;
    }
    $(".select-category").select2({
        placeholder: {
            id: '-1', // the value of the option
            text: 'Select an option'
        },
        allowClear: true,
        dropdownAutoWidth: true,
        width: '100%',
        templateResult: formatCategory,
        escapeMarkup: function (formatCategory) { return formatCategory; }
    });

    

    // datetimepick
    $('#datetimepicker').datetimepicker();
    $('#datetime-transfer').datetimepicker();


});


var ctx = document.getElementById('myChart');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["January", "February", "March", "April", "May", "June"],
        datasets: [
            {
                fillColor: "rgba(172,194,132,0.4)",
                strokeColor: "#ACC26D",
                pointColor: "#fff",
                pointStrokeColor: "#9DB86D",
                data: [203, 156, 99, 251, 305, 247]
            }
        ]

    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});